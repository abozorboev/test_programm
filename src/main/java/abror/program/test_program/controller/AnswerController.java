package abror.program.test_program.controller;

import abror.program.test_program.entity.Answer;
import abror.program.test_program.form.AnswerForm;
import abror.program.test_program.form.QuestionForm;
import abror.program.test_program.service.AnswerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/answer")
public class AnswerController {
    @Autowired
    AnswerService answerService;

    @GetMapping("/list")
    public List<Answer> getAllAnswer() {
        return answerService.getAllAnswer();
    }

    @PostMapping("/creative")
    public Answer creativeAnswer(@Valid @RequestBody AnswerForm answer) {
        return answerService.createAnswer(answer);
    }

    @GetMapping("/get/{id}")
    public Answer getAnswerById(@PathVariable(value = "id") Long answerId) {
        return answerService.getAnswerById(answerId);
    }

    @PutMapping("/update/{id}")
    public Answer updateAnswer(@PathVariable(value = "id") Long answerId,
                                   @Valid @RequestBody AnswerForm answerDetails) {
        return answerService.updateAnswer(answerId, answerDetails);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteAnswer(@PathVariable(value = "id") Long answerId) {
        return answerService.deleteAnswer(answerId);
    }
}
