package abror.program.test_program.controller;

import abror.program.test_program.entity.Question;
import abror.program.test_program.form.QuestionForm;
import abror.program.test_program.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/question")
public class QuestionController {


    @Autowired
    QuestionService questionService;

    @GetMapping("/list")
    public List<Question> getAllQuestion() {
        return questionService.getAllQuestion();
    }

    @PostMapping("/creative")
    public Question creativeQuestion(@Valid @RequestBody QuestionForm question) {
        return questionService.createQuestion(question);
    }

    @GetMapping("/get/{id}")
    public Question getQuestionById(@PathVariable(value = "id") Long questionId) {
        return questionService.getQuestionById(questionId);
    }

    @PutMapping("/update/{id}")
    public Question updateQuestion(@PathVariable(value = "id") Long questionId,
                                   @Valid @RequestBody QuestionForm questionDetails) {
        return questionService.updateQuestion(questionId, questionDetails);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteQuestion(@PathVariable(value = "id") Long questionId) {
        return questionService.deleteQuestion(questionId);
    }
}

