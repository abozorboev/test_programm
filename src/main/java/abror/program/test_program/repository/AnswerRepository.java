package abror.program.test_program.repository;

import abror.program.test_program.entity.Answer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AnswerRepository extends JpaRepository<Answer, Long> {
    Answer findByQuestionIdAndTitle(Long questionId, String title);
}
