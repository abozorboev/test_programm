package abror.program.test_program.service;

import abror.program.test_program.entity.Answer;
import abror.program.test_program.form.AnswerForm;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface AnswerService {
    List<Answer> getAllAnswer();

    Answer createAnswer(AnswerForm answer);

    Answer getAnswerById(Long answerId);

    Answer updateAnswer(Long answerId, AnswerForm answerDetails);

    ResponseEntity<?> deleteAnswer(Long answerId);
}
