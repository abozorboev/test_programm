package abror.program.test_program.service.Imp;

import abror.program.test_program.entity.Answer;
import abror.program.test_program.entity.Question;
import abror.program.test_program.expcetion.ResourceNotFoundException;
import abror.program.test_program.form.AnswerForm;
import abror.program.test_program.repository.AnswerRepository;
import abror.program.test_program.repository.QuestionRepository;
import abror.program.test_program.service.AnswerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AnswerServiceImp implements AnswerService {


    @Autowired
    AnswerRepository answerRepository;

    @Autowired
    QuestionRepository questionRepository;

    @Override
    public List<Answer> getAllAnswer() {
        return answerRepository.findAll();
    }


    @Override
    public Answer createAnswer(AnswerForm answerForm) {
        Question question = questionRepository.findById(answerForm.getQuestion_id())
                .orElseThrow(() -> new ResourceNotFoundException("Question", "id", answerForm.getQuestion_id()));

        Answer answer = new Answer();
        answer.setQuestion(question);
        answer.setTitle(answerForm.getTitle());
        answer.setIs_correct (answerForm.isIs_correct());
        answer.setText(answerForm.getText());

        return answerRepository.save(answer);
    }

    @Override
    public Answer getAnswerById(Long answerId) {
        return answerRepository.findById(answerId)
                .orElseThrow(() -> new ResourceNotFoundException("Answer", "id", answerId));
    }

    @Override
    public Answer updateAnswer(Long answerId, AnswerForm answerDetails) {

       Answer answer = answerRepository.findById(answerId)
                .orElseThrow(() -> new ResourceNotFoundException("Answer", "id", answerId));

       Question question = questionRepository.findById(answerDetails.getQuestion_id())
               .orElseThrow(() -> new ResourceNotFoundException("Question", "id", answerDetails.getQuestion_id()));

        answer.setText(answerDetails.getText());
        answer.setIs_correct(answerDetails.isIs_correct());
        answer.setTitle(answerDetails.getTitle());
        answer.setQuestion(question);
        return answerRepository.save(answer);
    }
    @Override
    public ResponseEntity<?> deleteAnswer(Long answerId) {
        Answer answer = answerRepository.findById(answerId)
                .orElseThrow(() -> new ResourceNotFoundException("answer", "id", answerId));

        answerRepository.delete(answer);

        return ResponseEntity.ok().build();
    }
}
