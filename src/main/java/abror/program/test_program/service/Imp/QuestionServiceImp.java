package abror.program.test_program.service.Imp;

import abror.program.test_program.entity.Answer;
import abror.program.test_program.entity.Question;
import abror.program.test_program.expcetion.ResourceNotFoundException;
import abror.program.test_program.form.QuestionForm;
import abror.program.test_program.repository.AnswerRepository;
import abror.program.test_program.repository.QuestionRepository;
import abror.program.test_program.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QuestionServiceImp implements QuestionService {

    @Autowired
    QuestionRepository questionRepository;

    @Autowired
    AnswerRepository answerRepository;

    @Override
    public List<Question> getAllQuestion() {
        return questionRepository.findAll();
    }


    @Override
    public Question createQuestion(QuestionForm questionForm) {
        Question question = new Question();
        question.setText(questionForm.getText());
        question.setDescripe(questionForm.getDescripe());

        question = questionRepository.save(question);

        Answer answer_A = new Answer();
        answer_A.setTitle("A)");
        answer_A.setText(questionForm.getAnswer_a());
        answer_A.setQuestion(question);
        answerRepository.save(answer_A);

        Answer answer_B = new Answer();
        answer_B.setTitle("B)");
        answer_B.setText(questionForm.getAnswer_b());
        answer_B.setQuestion(question);
        answerRepository.save(answer_B);

        Answer answer_C = new Answer();
        answer_C.setTitle("C)");
        answer_C.setText(questionForm.getAnswer_c());
        answer_C.setQuestion(question);
        answerRepository.save(answer_C);

        Answer answer_D = new Answer();
        answer_D.setTitle("D)");
        answer_D.setText(questionForm.getAnswer_d());
        answer_D.setQuestion(question);
        answerRepository.save(answer_D);

        return question;
    }

    @Override
    public Question getQuestionById(Long questionId) {
        return questionRepository.findById(questionId)
                .orElseThrow(() -> new ResourceNotFoundException("Question", "id", questionId));
    }

    @Override
    public Question updateQuestion(Long questionId, QuestionForm questionDetails) {

        Question question = questionRepository.findById(questionId)
                .orElseThrow(() -> new ResourceNotFoundException("Question", "id", questionId));
        question.setText(questionDetails.getText());
        question.setDescripe(questionDetails.getDescripe());

        Answer answer_A = answerRepository.findByQuestionIdAndTitle(questionId, "A)");
        answer_A.setText(questionDetails.getAnswer_a());
        answerRepository.save(answer_A);

        Answer answer_B = answerRepository.findByQuestionIdAndTitle(questionId, "B)");
        answer_A.setText(questionDetails.getAnswer_b());
        answerRepository.save(answer_B);

        Answer answer_C = answerRepository.findByQuestionIdAndTitle(questionId, "C)");
        answer_C.setText(questionDetails.getAnswer_c());
        answerRepository.save(answer_C);

        Answer answer_D = answerRepository.findByQuestionIdAndTitle(questionId, "D)");
        answer_D.setText(questionDetails.getAnswer_d());
        answerRepository.save(answer_D);

        return questionRepository.save(question);

    }
    @Override
    public ResponseEntity<?> deleteQuestion(Long questionId) {
        Question question = questionRepository.findById(questionId)
                .orElseThrow(() -> new ResourceNotFoundException("question", "id", questionId));

        questionRepository.delete(question);
        Answer answer_A = answerRepository.findByQuestionIdAndTitle(questionId, "A)");
        answerRepository.delete(answer_A);

        Answer answer_B = answerRepository.findByQuestionIdAndTitle(questionId, "B)");
        answerRepository.delete(answer_B);

        Answer answer_C = answerRepository.findByQuestionIdAndTitle(questionId, "C)");
        answerRepository.delete(answer_C);

        Answer answer_D = answerRepository.findByQuestionIdAndTitle(questionId, "D)");
        answerRepository.delete(answer_D);
        return ResponseEntity.ok().build();
    }

}
