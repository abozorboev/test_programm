package abror.program.test_program.service;

import abror.program.test_program.entity.Question;
import abror.program.test_program.form.QuestionForm;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface QuestionService {
    List<Question> getAllQuestion();

    Question createQuestion(QuestionForm question);

    Question getQuestionById(Long questionId);

    Question updateQuestion(Long questionId, QuestionForm questionDetails);

    ResponseEntity<?> deleteQuestion(Long questionId);
}
